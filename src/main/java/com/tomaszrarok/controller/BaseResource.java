package com.tomaszrarok.controller;

import javax.ws.rs.core.Response;

public abstract class BaseResource {
    protected Response getExceptionResponse(String message){
        Response.ResponseBuilder resp_builder= Response.status(Response.Status.BAD_REQUEST);
        resp_builder.entity(message);//message you need as the body
        return resp_builder.build();
    }
}
