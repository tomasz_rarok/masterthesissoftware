package com.tomaszrarok.controller;

import com.tomaszrarok.generated.controller.AbstractCategoryResource;

import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
 * REST API for Category
 */
@Path("/category")
@Produces({"text/xml", "application/json"})
@Consumes({"text/xml", "application/json"})
public  class CategoryResource extends AbstractCategoryResource  {

}