package com.tomaszrarok.controller;

import com.tomaszrarok.generated.controller.AbstractProductsResource;

import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
 * REST API for Products
 */
@Path("/products")
@Produces({"text/xml", "application/json"})
@Consumes({"text/xml", "application/json"})
public  class ProductsResource extends AbstractProductsResource  {

}