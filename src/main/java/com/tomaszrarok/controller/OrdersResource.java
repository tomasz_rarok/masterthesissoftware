package com.tomaszrarok.controller;

import com.tomaszrarok.generated.controller.AbstractOrdersResource;

import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
 * REST API for Orders
 */
@Path("/orders")
@Produces({"text/xml", "application/json"})
@Consumes({"text/xml", "application/json"})
public  class OrdersResource extends AbstractOrdersResource  {

}