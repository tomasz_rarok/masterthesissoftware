package com.tomaszrarok.controller;

import com.tomaszrarok.generated.controller.AbstractCustomersResource;

import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
 * REST API for Customers
 */
@Path("/customers")
@Produces({"text/xml", "application/json"})
@Consumes({"text/xml", "application/json"})
public  class CustomersResource extends AbstractCustomersResource  {

}