package com.tomaszrarok.controller;

import com.tomaszrarok.generated.controller.AbstractOrderproductResource;

import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
 * REST API for Orderproduct
 */
@Path("/orderproduct")
@Produces({"text/xml", "application/json"})
@Consumes({"text/xml", "application/json"})
public  class OrderproductResource extends AbstractOrderproductResource  {

}