package com.tomaszrarok.filter;

public class BaseFilter {
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
