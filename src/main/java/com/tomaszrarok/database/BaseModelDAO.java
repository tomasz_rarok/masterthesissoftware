package com.tomaszrarok.database;


import com.tomaszrarok.model.BaseModel;

        import java.io.Closeable;
        import java.util.List;
        import java.util.logging.Level;
        import java.util.logging.Logger;
        import javax.naming.InitialContext;
        import javax.persistence.EntityManager;
        import javax.persistence.Query;
        import javax.transaction.Status;
        import javax.transaction.UserTransaction;

public abstract class BaseModelDAO<T extends BaseModel> implements Closeable {

    protected boolean transactionInitiated = false;
    protected UserTransaction utx = null;
    protected EntityManager em;
    protected Class<T> entityClass;

    public BaseModelDAO(Class<T> entityClass) {
        this.entityClass = entityClass;
        try {
            em = (EntityManager) (new InitialContext())
                    .lookup("java:comp/env/persistence/em");
            utx = (UserTransaction) (new InitialContext())
                    .lookup("java:comp/UserTransaction");
            if (utx.getStatus() != Status.STATUS_ACTIVE) {
                utx.begin();
                transactionInitiated = true;
            }
        } catch (Exception ex) {
            throw new RuntimeException("Could not connect to database.", ex);
        }
    }

    public T get(Long id)  {

        // try to get the entity from the database
        T entity = em.find(entityClass, id);

        // check if entity exists
        if (entity == null) {
            return null;
        }

        // entity exists, return it
        return entity;
    }

    public T save(T entity) {

        if (entity.getId() != null) {
            entity = em.merge(entity);
        } else {
            em.persist(entity);
        }

        return entity;
    }

    public List<T> list() {
        Query q = em.createQuery("SELECT i FROM "
                + entityClass.getSimpleName() + " i");
        return q.getResultList();
    }

    public void delete(T entity) {
        entity = em.merge(entity);
        em.remove(entity);
    }

    @Override
    public void close() {
        try {
            if (utx.getStatus() == Status.STATUS_ACTIVE && transactionInitiated) {
                utx.commit();
            }
        } catch (Exception ex) {
            Logger.getLogger(BaseModelDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}