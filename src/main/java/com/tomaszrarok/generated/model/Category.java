package com.tomaszrarok.generated.model;

import javax.persistence.*;

/**
 * Model class for category
 */
@Entity
@Table(name = "category")
public class Category extends AbstractCategory {
    private static final long serialVersionUID = 1L;
}