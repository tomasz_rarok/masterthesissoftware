package com.tomaszrarok.generated.model;

import javax.persistence.*;

/**
 * Model class for products
 */
@Entity
@Table(name = "products")
public class Products extends AbstractProducts {
    private static final long serialVersionUID = 1L;
}