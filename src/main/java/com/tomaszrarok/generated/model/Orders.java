package com.tomaszrarok.generated.model;

import javax.persistence.*;

/**
 * Model class for orders
 */
@Entity
@Table(name = "orders")
public class Orders extends AbstractOrders {
    private static final long serialVersionUID = 1L;
}