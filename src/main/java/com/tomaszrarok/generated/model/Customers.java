package com.tomaszrarok.generated.model;

import javax.persistence.*;

/**
 * Model class for customers
 */
@Entity
@Table(name = "customers")
public class Customers extends AbstractCustomers {
    private static final long serialVersionUID = 1L;
}