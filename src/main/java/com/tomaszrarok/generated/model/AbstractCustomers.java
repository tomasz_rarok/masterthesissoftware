package com.tomaszrarok.generated.model;

import com.tomaszrarok.model.BaseModel;
import javax.persistence.*;
/**
 * Model class for customers
 */
@MappedSuperclass
abstract class AbstractCustomers extends BaseModel {

private static final long serialVersionUID = 1L;

  @Column(name = "firstname")
  private String firstname;

  @Column(name = "lastname")
  private String lastname;

  @Column(name = "email")
  private String email;

  @Column(name = "username")
  private String username;

  @Column(name = "password")
  private String password;

  
  
  /**
   * Getter for firstname
   * @return the firstname
   */
  public String getFirstname() {
    return firstname;
  }

  /**
   * Setter for firstname
   * @param firstname the firstname to set
   */
  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }
  
  /**
   * Getter for lastname
   * @return the lastname
   */
  public String getLastname() {
    return lastname;
  }

  /**
   * Setter for lastname
   * @param lastname the lastname to set
   */
  public void setLastname(String lastname) {
    this.lastname = lastname;
  }
  
  /**
   * Getter for email
   * @return the email
   */
  public String getEmail() {
    return email;
  }

  /**
   * Setter for email
   * @param email the email to set
   */
  public void setEmail(String email) {
    this.email = email;
  }
  
  /**
   * Getter for username
   * @return the username
   */
  public String getUsername() {
    return username;
  }

  /**
   * Setter for username
   * @param username the username to set
   */
  public void setUsername(String username) {
    this.username = username;
  }
  
  /**
   * Getter for password
   * @return the password
   */
  public String getPassword() {
    return password;
  }

  /**
   * Setter for password
   * @param password the password to set
   */
  public void setPassword(String password) {
    this.password = password;
  }
  
}