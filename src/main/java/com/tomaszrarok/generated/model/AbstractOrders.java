package com.tomaszrarok.generated.model;

import com.tomaszrarok.model.BaseModel;
import javax.persistence.*;
/**
 * Model class for orders
 */
@MappedSuperclass
abstract class AbstractOrders extends BaseModel {

private static final long serialVersionUID = 1L;

  @Column(name = "name")
  private String name;

  
  @ManyToOne
  @JoinColumn(name = "customers_id")
  private Customers customers;

  
  
  /**
   * Getter for name
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * Setter for name
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }
  
  /**
   * Getter for customers
   * @return the customers
   */
  public Customers getCustomers() {
    return customers;
  }

  /**
   * Setter for customers
   * @param customers the customers to set
   */
  public void setCustomers(Customers customers) {
    this.customers = customers;
  }
  
}