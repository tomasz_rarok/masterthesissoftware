package com.tomaszrarok.generated.model;

import com.tomaszrarok.model.BaseModel;
import javax.persistence.*;
/**
 * Model class for products
 */
@MappedSuperclass
abstract class AbstractProducts extends BaseModel {

private static final long serialVersionUID = 1L;

  @Column(name = "name")
  private String name;

  @Column(name = "price")
  private Double price;

  
  @ManyToOne
  @JoinColumn(name = "category_id")
  private Category category;

  
  
  /**
   * Getter for name
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * Setter for name
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }
  
  /**
   * Getter for price
   * @return the price
   */
  public Double getPrice() {
    return price;
  }

  /**
   * Setter for price
   * @param price the price to set
   */
  public void setPrice(Double price) {
    this.price = price;
  }
  
  /**
   * Getter for category
   * @return the category
   */
  public Category getCategory() {
    return category;
  }

  /**
   * Setter for category
   * @param category the category to set
   */
  public void setCategory(Category category) {
    this.category = category;
  }
  
}