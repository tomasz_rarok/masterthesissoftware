package com.tomaszrarok.generated.model;

import com.tomaszrarok.model.BaseModel;
import javax.persistence.*;
/**
 * Model class for orderproduct
 */
@MappedSuperclass
abstract class AbstractOrderproduct extends BaseModel {

private static final long serialVersionUID = 1L;

  @Column(name = "name")
  private Double name;

  
  @ManyToOne
  @JoinColumn(name = "products_id")
  private Products products;

  
  @ManyToOne
  @JoinColumn(name = "orders_id")
  private Orders orders;

  
  
  /**
   * Getter for name
   * @return the name
   */
  public Double getName() {
    return name;
  }

  /**
   * Setter for name
   * @param name the name to set
   */
  public void setName(Double name) {
    this.name = name;
  }
  
  /**
   * Getter for products
   * @return the products
   */
  public Products getProducts() {
    return products;
  }

  /**
   * Setter for products
   * @param products the products to set
   */
  public void setProducts(Products products) {
    this.products = products;
  }
  
  /**
   * Getter for orders
   * @return the orders
   */
  public Orders getOrders() {
    return orders;
  }

  /**
   * Setter for orders
   * @param orders the orders to set
   */
  public void setOrders(Orders orders) {
    this.orders = orders;
  }
  
}