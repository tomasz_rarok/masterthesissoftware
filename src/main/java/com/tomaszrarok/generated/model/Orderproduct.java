package com.tomaszrarok.generated.model;

import javax.persistence.*;

/**
 * Model class for orderproduct
 */
@Entity
@Table(name = "orderproduct")
public class Orderproduct extends AbstractOrderproduct {
    private static final long serialVersionUID = 1L;
}