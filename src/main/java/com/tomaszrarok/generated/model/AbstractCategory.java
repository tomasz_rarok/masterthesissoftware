package com.tomaszrarok.generated.model;

import com.tomaszrarok.model.BaseModel;
import javax.persistence.*;
/**
 * Model class for category
 */
@MappedSuperclass
abstract class AbstractCategory extends BaseModel {

private static final long serialVersionUID = 1L;

  @Column(name = "name")
  private String name;

  
  
  /**
   * Getter for name
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * Setter for name
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }
  
}