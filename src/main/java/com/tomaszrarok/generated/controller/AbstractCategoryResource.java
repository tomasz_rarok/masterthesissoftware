package com.tomaszrarok.generated.controller;

import com.tomaszrarok.controller.BaseResource;
import com.tomaszrarok.generated.database.Categorys;
import com.tomaszrarok.generated.model.Category;
import com.tomaszrarok.generated.validator.CategoryValidator;
import com.tomaszrarok.generated.filter.CategoryFilter;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * REST API for Category
 */
public abstract class AbstractCategoryResource extends BaseResource {

    /**
     * Get a list of categorys.
     *
     * @param request the current HTTP request
     * @return a list of categorys
     */
    @GET
    public List<Category> index(@Context HttpServletRequest request) {

        try (Categorys categorys = new Categorys()) {
            return categorys.list();
        }
    }

    /**
     * Get a filtered list of  categorys.
     * @param filter the category filter
     * @param request the current HTTP request
     * @return a list of category
     */
    @POST
    @Path("/filter")
    public List<Category> filter(CategoryFilter filter,
                                         @Context HttpServletRequest request) {

        try (Categorys categorys= new Categorys ()) {

            return categorys.list(filter);
        }
    }

    /**
     * Get a single category.
     *
     * @param id      the id of the category
     * @param request the current HTTP request
     * @return the category
     */
    @GET
    @Path("/{id}")
    public Category details(@PathParam("id") Long id,
                            @Context HttpServletRequest request) {

        try (Categorys categorys = new Categorys()) {
            return categorys.get(id);
        }
    }

    /**
     * Creates a new category.
     *
     * @param category the new category
     * @param request  the current HTTP request
     * @return the created category
     */
    @POST
    public Category create(Category category,
                           @Context HttpServletRequest request) {
        // validate input
        CategoryValidator categoryValidator = new CategoryValidator();
        Map<String, List<String>> errors = categoryValidator.validate(category);

        if (!errors.isEmpty()) {
            throw new WebApplicationException(getExceptionResponse(errors.toString()));
        }

        // if item exists, do update
        boolean exists = false;
        if (category.getId() != null) {
            try (Categorys categorys = new Categorys();) {
                exists = categorys.exists(category.getId());
            }
        }

        if (exists) {
            return update(category.getId(), category, request);
        }

        // ... else create a new one
        try (Categorys categorys = new Categorys();) {
            category.setCreateDate(new Date());
            category.setModifyDate(category.getCreateDate());
            // save and return result
            return categorys.save(category);
        }
    }

    /**
     * Saves a changed category.
     *
     * @param id       the id of the category
     * @param category the updated contract
     * @param request  the current HTTP request
     * @return the updated category object
     */
    @PUT
    @Path("/{id}")
    public Category update(@PathParam("id") Long id, Category category,
                           @Context HttpServletRequest request) {

        try (Categorys categorys = new Categorys();) {

            // validate input
            CategoryValidator categoryValidator = new CategoryValidator();
            Map<String, List<String>> errors = categoryValidator.validate(category);

            if (!errors.isEmpty()) {
                throw new WebApplicationException(getExceptionResponse(errors.toString()));
            }

            // check if item exists
            Category dbCategory = categorys.get(id);

            // copy data to database item

              dbCategory.setName(category.getName());
             
            dbCategory.setModifyDate(new Date());

            //save and return result
            return categorys.save(dbCategory);

        }
    }

    /**
     * Deletes a category by id.
     *
     * @param id      the uuid of the contract
     * @param request the current HTTP request
     */
    @DELETE
    @Path("/{id}")
    public void delete(@PathParam("id") Long id, @Context HttpServletRequest request) {
        try (Categorys categorys = new Categorys()) {

            // get category
            Category category = categorys.get(id);

            // delete category
            categorys.delete(category);

        }
    }
}