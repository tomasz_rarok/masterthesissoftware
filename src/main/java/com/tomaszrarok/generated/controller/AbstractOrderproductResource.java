package com.tomaszrarok.generated.controller;

import com.tomaszrarok.controller.BaseResource;
import com.tomaszrarok.generated.database.Orderproducts;
import com.tomaszrarok.generated.model.Orderproduct;
import com.tomaszrarok.generated.validator.OrderproductValidator;
import com.tomaszrarok.generated.filter.OrderproductFilter;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * REST API for Orderproduct
 */
public abstract class AbstractOrderproductResource extends BaseResource {

    /**
     * Get a list of orderproducts.
     *
     * @param request the current HTTP request
     * @return a list of orderproducts
     */
    @GET
    public List<Orderproduct> index(@Context HttpServletRequest request) {

        try (Orderproducts orderproducts = new Orderproducts()) {
            return orderproducts.list();
        }
    }

    /**
     * Get a filtered list of  orderproducts.
     * @param filter the orderproduct filter
     * @param request the current HTTP request
     * @return a list of orderproduct
     */
    @POST
    @Path("/filter")
    public List<Orderproduct> filter(OrderproductFilter filter,
                                         @Context HttpServletRequest request) {

        try (Orderproducts orderproducts= new Orderproducts ()) {

            return orderproducts.list(filter);
        }
    }

    /**
     * Get a single orderproduct.
     *
     * @param id      the id of the orderproduct
     * @param request the current HTTP request
     * @return the orderproduct
     */
    @GET
    @Path("/{id}")
    public Orderproduct details(@PathParam("id") Long id,
                            @Context HttpServletRequest request) {

        try (Orderproducts orderproducts = new Orderproducts()) {
            return orderproducts.get(id);
        }
    }

    /**
     * Creates a new orderproduct.
     *
     * @param orderproduct the new orderproduct
     * @param request  the current HTTP request
     * @return the created orderproduct
     */
    @POST
    public Orderproduct create(Orderproduct orderproduct,
                           @Context HttpServletRequest request) {
        // validate input
        OrderproductValidator orderproductValidator = new OrderproductValidator();
        Map<String, List<String>> errors = orderproductValidator.validate(orderproduct);

        if (!errors.isEmpty()) {
            throw new WebApplicationException(getExceptionResponse(errors.toString()));
        }

        // if item exists, do update
        boolean exists = false;
        if (orderproduct.getId() != null) {
            try (Orderproducts orderproducts = new Orderproducts();) {
                exists = orderproducts.exists(orderproduct.getId());
            }
        }

        if (exists) {
            return update(orderproduct.getId(), orderproduct, request);
        }

        // ... else create a new one
        try (Orderproducts orderproducts = new Orderproducts();) {
            orderproduct.setCreateDate(new Date());
            orderproduct.setModifyDate(orderproduct.getCreateDate());
            // save and return result
            return orderproducts.save(orderproduct);
        }
    }

    /**
     * Saves a changed orderproduct.
     *
     * @param id       the id of the orderproduct
     * @param orderproduct the updated contract
     * @param request  the current HTTP request
     * @return the updated orderproduct object
     */
    @PUT
    @Path("/{id}")
    public Orderproduct update(@PathParam("id") Long id, Orderproduct orderproduct,
                           @Context HttpServletRequest request) {

        try (Orderproducts orderproducts = new Orderproducts();) {

            // validate input
            OrderproductValidator orderproductValidator = new OrderproductValidator();
            Map<String, List<String>> errors = orderproductValidator.validate(orderproduct);

            if (!errors.isEmpty()) {
                throw new WebApplicationException(getExceptionResponse(errors.toString()));
            }

            // check if item exists
            Orderproduct dbOrderproduct = orderproducts.get(id);

            // copy data to database item

              dbOrderproduct.setName(orderproduct.getName());
              dbOrderproduct.setProducts(orderproduct.getProducts());
              dbOrderproduct.setOrders(orderproduct.getOrders());
             
            dbOrderproduct.setModifyDate(new Date());

            //save and return result
            return orderproducts.save(dbOrderproduct);

        }
    }

    /**
     * Deletes a orderproduct by id.
     *
     * @param id      the uuid of the contract
     * @param request the current HTTP request
     */
    @DELETE
    @Path("/{id}")
    public void delete(@PathParam("id") Long id, @Context HttpServletRequest request) {
        try (Orderproducts orderproducts = new Orderproducts()) {

            // get orderproduct
            Orderproduct orderproduct = orderproducts.get(id);

            // delete orderproduct
            orderproducts.delete(orderproduct);

        }
    }
}