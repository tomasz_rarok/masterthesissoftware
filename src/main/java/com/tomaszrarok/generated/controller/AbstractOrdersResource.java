package com.tomaszrarok.generated.controller;

import com.tomaszrarok.controller.BaseResource;
import com.tomaszrarok.generated.database.Orderss;
import com.tomaszrarok.generated.model.Orders;
import com.tomaszrarok.generated.validator.OrdersValidator;
import com.tomaszrarok.generated.filter.OrdersFilter;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * REST API for Orders
 */
public abstract class AbstractOrdersResource extends BaseResource {

    /**
     * Get a list of orderss.
     *
     * @param request the current HTTP request
     * @return a list of orderss
     */
    @GET
    public List<Orders> index(@Context HttpServletRequest request) {

        try (Orderss orderss = new Orderss()) {
            return orderss.list();
        }
    }

    /**
     * Get a filtered list of  orderss.
     * @param filter the orders filter
     * @param request the current HTTP request
     * @return a list of orders
     */
    @POST
    @Path("/filter")
    public List<Orders> filter(OrdersFilter filter,
                                         @Context HttpServletRequest request) {

        try (Orderss orderss= new Orderss ()) {

            return orderss.list(filter);
        }
    }

    /**
     * Get a single orders.
     *
     * @param id      the id of the orders
     * @param request the current HTTP request
     * @return the orders
     */
    @GET
    @Path("/{id}")
    public Orders details(@PathParam("id") Long id,
                            @Context HttpServletRequest request) {

        try (Orderss orderss = new Orderss()) {
            return orderss.get(id);
        }
    }

    /**
     * Creates a new orders.
     *
     * @param orders the new orders
     * @param request  the current HTTP request
     * @return the created orders
     */
    @POST
    public Orders create(Orders orders,
                           @Context HttpServletRequest request) {
        // validate input
        OrdersValidator ordersValidator = new OrdersValidator();
        Map<String, List<String>> errors = ordersValidator.validate(orders);

        if (!errors.isEmpty()) {
            throw new WebApplicationException(getExceptionResponse(errors.toString()));
        }

        // if item exists, do update
        boolean exists = false;
        if (orders.getId() != null) {
            try (Orderss orderss = new Orderss();) {
                exists = orderss.exists(orders.getId());
            }
        }

        if (exists) {
            return update(orders.getId(), orders, request);
        }

        // ... else create a new one
        try (Orderss orderss = new Orderss();) {
            orders.setCreateDate(new Date());
            orders.setModifyDate(orders.getCreateDate());
            // save and return result
            return orderss.save(orders);
        }
    }

    /**
     * Saves a changed orders.
     *
     * @param id       the id of the orders
     * @param orders the updated contract
     * @param request  the current HTTP request
     * @return the updated orders object
     */
    @PUT
    @Path("/{id}")
    public Orders update(@PathParam("id") Long id, Orders orders,
                           @Context HttpServletRequest request) {

        try (Orderss orderss = new Orderss();) {

            // validate input
            OrdersValidator ordersValidator = new OrdersValidator();
            Map<String, List<String>> errors = ordersValidator.validate(orders);

            if (!errors.isEmpty()) {
                throw new WebApplicationException(getExceptionResponse(errors.toString()));
            }

            // check if item exists
            Orders dbOrders = orderss.get(id);

            // copy data to database item

              dbOrders.setName(orders.getName());
              dbOrders.setCustomers(orders.getCustomers());
             
            dbOrders.setModifyDate(new Date());

            //save and return result
            return orderss.save(dbOrders);

        }
    }

    /**
     * Deletes a orders by id.
     *
     * @param id      the uuid of the contract
     * @param request the current HTTP request
     */
    @DELETE
    @Path("/{id}")
    public void delete(@PathParam("id") Long id, @Context HttpServletRequest request) {
        try (Orderss orderss = new Orderss()) {

            // get orders
            Orders orders = orderss.get(id);

            // delete orders
            orderss.delete(orders);

        }
    }
}