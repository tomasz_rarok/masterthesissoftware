package com.tomaszrarok.generated.controller;

import com.tomaszrarok.controller.BaseResource;
import com.tomaszrarok.generated.database.Productss;
import com.tomaszrarok.generated.model.Products;
import com.tomaszrarok.generated.validator.ProductsValidator;
import com.tomaszrarok.generated.filter.ProductsFilter;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * REST API for Products
 */
public abstract class AbstractProductsResource extends BaseResource {

    /**
     * Get a list of productss.
     *
     * @param request the current HTTP request
     * @return a list of productss
     */
    @GET
    public List<Products> index(@Context HttpServletRequest request) {

        try (Productss productss = new Productss()) {
            return productss.list();
        }
    }

    /**
     * Get a filtered list of  productss.
     * @param filter the products filter
     * @param request the current HTTP request
     * @return a list of products
     */
    @POST
    @Path("/filter")
    public List<Products> filter(ProductsFilter filter,
                                         @Context HttpServletRequest request) {

        try (Productss productss= new Productss ()) {

            return productss.list(filter);
        }
    }

    /**
     * Get a single products.
     *
     * @param id      the id of the products
     * @param request the current HTTP request
     * @return the products
     */
    @GET
    @Path("/{id}")
    public Products details(@PathParam("id") Long id,
                            @Context HttpServletRequest request) {

        try (Productss productss = new Productss()) {
            return productss.get(id);
        }
    }

    /**
     * Creates a new products.
     *
     * @param products the new products
     * @param request  the current HTTP request
     * @return the created products
     */
    @POST
    public Products create(Products products,
                           @Context HttpServletRequest request) {
        // validate input
        ProductsValidator productsValidator = new ProductsValidator();
        Map<String, List<String>> errors = productsValidator.validate(products);

        if (!errors.isEmpty()) {
            throw new WebApplicationException(getExceptionResponse(errors.toString()));
        }

        // if item exists, do update
        boolean exists = false;
        if (products.getId() != null) {
            try (Productss productss = new Productss();) {
                exists = productss.exists(products.getId());
            }
        }

        if (exists) {
            return update(products.getId(), products, request);
        }

        // ... else create a new one
        try (Productss productss = new Productss();) {
            products.setCreateDate(new Date());
            products.setModifyDate(products.getCreateDate());
            // save and return result
            return productss.save(products);
        }
    }

    /**
     * Saves a changed products.
     *
     * @param id       the id of the products
     * @param products the updated contract
     * @param request  the current HTTP request
     * @return the updated products object
     */
    @PUT
    @Path("/{id}")
    public Products update(@PathParam("id") Long id, Products products,
                           @Context HttpServletRequest request) {

        try (Productss productss = new Productss();) {

            // validate input
            ProductsValidator productsValidator = new ProductsValidator();
            Map<String, List<String>> errors = productsValidator.validate(products);

            if (!errors.isEmpty()) {
                throw new WebApplicationException(getExceptionResponse(errors.toString()));
            }

            // check if item exists
            Products dbProducts = productss.get(id);

            // copy data to database item

              dbProducts.setName(products.getName());
              dbProducts.setPrice(products.getPrice());
              dbProducts.setCategory(products.getCategory());
             
            dbProducts.setModifyDate(new Date());

            //save and return result
            return productss.save(dbProducts);

        }
    }

    /**
     * Deletes a products by id.
     *
     * @param id      the uuid of the contract
     * @param request the current HTTP request
     */
    @DELETE
    @Path("/{id}")
    public void delete(@PathParam("id") Long id, @Context HttpServletRequest request) {
        try (Productss productss = new Productss()) {

            // get products
            Products products = productss.get(id);

            // delete products
            productss.delete(products);

        }
    }
}