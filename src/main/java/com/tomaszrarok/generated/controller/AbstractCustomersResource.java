package com.tomaszrarok.generated.controller;

import com.tomaszrarok.controller.BaseResource;
import com.tomaszrarok.generated.database.Customerss;
import com.tomaszrarok.generated.model.Customers;
import com.tomaszrarok.generated.validator.CustomersValidator;
import com.tomaszrarok.generated.filter.CustomersFilter;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * REST API for Customers
 */
public abstract class AbstractCustomersResource extends BaseResource {

    /**
     * Get a list of customerss.
     *
     * @param request the current HTTP request
     * @return a list of customerss
     */
    @GET
    public List<Customers> index(@Context HttpServletRequest request) {

        try (Customerss customerss = new Customerss()) {
            return customerss.list();
        }
    }

    /**
     * Get a filtered list of  customerss.
     * @param filter the customers filter
     * @param request the current HTTP request
     * @return a list of customers
     */
    @POST
    @Path("/filter")
    public List<Customers> filter(CustomersFilter filter,
                                         @Context HttpServletRequest request) {

        try (Customerss customerss= new Customerss ()) {

            return customerss.list(filter);
        }
    }

    /**
     * Get a single customers.
     *
     * @param id      the id of the customers
     * @param request the current HTTP request
     * @return the customers
     */
    @GET
    @Path("/{id}")
    public Customers details(@PathParam("id") Long id,
                            @Context HttpServletRequest request) {

        try (Customerss customerss = new Customerss()) {
            return customerss.get(id);
        }
    }

    /**
     * Creates a new customers.
     *
     * @param customers the new customers
     * @param request  the current HTTP request
     * @return the created customers
     */
    @POST
    public Customers create(Customers customers,
                           @Context HttpServletRequest request) {
        // validate input
        CustomersValidator customersValidator = new CustomersValidator();
        Map<String, List<String>> errors = customersValidator.validate(customers);

        if (!errors.isEmpty()) {
            throw new WebApplicationException(getExceptionResponse(errors.toString()));
        }

        // if item exists, do update
        boolean exists = false;
        if (customers.getId() != null) {
            try (Customerss customerss = new Customerss();) {
                exists = customerss.exists(customers.getId());
            }
        }

        if (exists) {
            return update(customers.getId(), customers, request);
        }

        // ... else create a new one
        try (Customerss customerss = new Customerss();) {
            customers.setCreateDate(new Date());
            customers.setModifyDate(customers.getCreateDate());
            // save and return result
            return customerss.save(customers);
        }
    }

    /**
     * Saves a changed customers.
     *
     * @param id       the id of the customers
     * @param customers the updated contract
     * @param request  the current HTTP request
     * @return the updated customers object
     */
    @PUT
    @Path("/{id}")
    public Customers update(@PathParam("id") Long id, Customers customers,
                           @Context HttpServletRequest request) {

        try (Customerss customerss = new Customerss();) {

            // validate input
            CustomersValidator customersValidator = new CustomersValidator();
            Map<String, List<String>> errors = customersValidator.validate(customers);

            if (!errors.isEmpty()) {
                throw new WebApplicationException(getExceptionResponse(errors.toString()));
            }

            // check if item exists
            Customers dbCustomers = customerss.get(id);

            // copy data to database item

              dbCustomers.setFirstname(customers.getFirstname());
              dbCustomers.setLastname(customers.getLastname());
              dbCustomers.setEmail(customers.getEmail());
              dbCustomers.setUsername(customers.getUsername());
              dbCustomers.setPassword(customers.getPassword());
             
            dbCustomers.setModifyDate(new Date());

            //save and return result
            return customerss.save(dbCustomers);

        }
    }

    /**
     * Deletes a customers by id.
     *
     * @param id      the uuid of the contract
     * @param request the current HTTP request
     */
    @DELETE
    @Path("/{id}")
    public void delete(@PathParam("id") Long id, @Context HttpServletRequest request) {
        try (Customerss customerss = new Customerss()) {

            // get customers
            Customers customers = customerss.get(id);

            // delete customers
            customerss.delete(customers);

        }
    }
}