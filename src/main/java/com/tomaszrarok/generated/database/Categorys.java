package com.tomaszrarok.generated.database;

import com.tomaszrarok.database.BaseModelDAO;
import com.tomaszrarok.generated.model.Category;

import com.tomaszrarok.generated.filter.CategoryFilter;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Data access class for category
 */
public class Categorys extends BaseModelDAO<Category> {

    /**
     * Creates a new instance of category DAO.
     */
    public Categorys() {
        super(Category.class);
    }

    /**
     * Test if the category with the given id exists.
     *
     * @param id the id of the category
     * @return true if category exists
     */
    public boolean exists(Long id) {

        // check database connection
        assert em.isOpen();

        // query for elements
        Query q = em.createQuery("SELECT a FROM Category a WHERE "
                + "a.id = :id");
        q.setParameter("id", id);

        // return result
        return !q.getResultList().isEmpty();
    }


    /**
     * Removes a category and unbounds it from foreign relations.
     *
     * @param category the category
     */
    @Override
    public void delete(Category category) {


        // backup primary key
        Long id = category.getId();

        // update entity and remove
        category = get(id);
        if (category != null) super.delete(category);
    }

    public List<Category> list(CategoryFilter filter) {
            // check database connection
            assert em.isOpen();

            // build query using criteria API
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery cq = cb.createQuery();

            // get root
            Root<Category> entity = cq.from(Category.class);

            // prepare where list
            ArrayList<Predicate> where = new ArrayList<>();

            // filter by id?
            if (filter.getId() != null) {
                Path<Long> id = entity.get("id");
                where.add(cb.equal(id, filter.getId()));
            }

            // filter by name?
            if (filter.getName() != null) {
                Path<String> name = entity.get("name");
                where.add(cb.equal(name, filter.getName()));
            }
            

            // add where clause
            cq.where(cb.and(where.toArray(new Predicate[where.size()])));

            // compile query
            TypedQuery<Category> typedQuery = em.createQuery(cq);

            // get list
            List<Category> resultList = typedQuery.getResultList();

            // build result object
            return resultList;
        }
}
