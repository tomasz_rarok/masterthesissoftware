package com.tomaszrarok.generated.database;

import com.tomaszrarok.database.BaseModelDAO;
import com.tomaszrarok.generated.model.Customers;

import com.tomaszrarok.generated.filter.CustomersFilter;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Data access class for customers
 */
public class Customerss extends BaseModelDAO<Customers> {

    /**
     * Creates a new instance of customers DAO.
     */
    public Customerss() {
        super(Customers.class);
    }

    /**
     * Test if the customers with the given id exists.
     *
     * @param id the id of the customers
     * @return true if customers exists
     */
    public boolean exists(Long id) {

        // check database connection
        assert em.isOpen();

        // query for elements
        Query q = em.createQuery("SELECT a FROM Customers a WHERE "
                + "a.id = :id");
        q.setParameter("id", id);

        // return result
        return !q.getResultList().isEmpty();
    }


    /**
     * Removes a customers and unbounds it from foreign relations.
     *
     * @param customers the customers
     */
    @Override
    public void delete(Customers customers) {


        // backup primary key
        Long id = customers.getId();

        // update entity and remove
        customers = get(id);
        if (customers != null) super.delete(customers);
    }

    public List<Customers> list(CustomersFilter filter) {
            // check database connection
            assert em.isOpen();

            // build query using criteria API
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery cq = cb.createQuery();

            // get root
            Root<Customers> entity = cq.from(Customers.class);

            // prepare where list
            ArrayList<Predicate> where = new ArrayList<>();

            // filter by id?
            if (filter.getId() != null) {
                Path<Long> id = entity.get("id");
                where.add(cb.equal(id, filter.getId()));
            }

            // filter by firstname?
            if (filter.getFirstname() != null) {
                Path<String> firstname = entity.get("firstname");
                where.add(cb.equal(firstname, filter.getFirstname()));
            }
            // filter by lastname?
            if (filter.getLastname() != null) {
                Path<String> lastname = entity.get("lastname");
                where.add(cb.equal(lastname, filter.getLastname()));
            }
            // filter by email?
            if (filter.getEmail() != null) {
                Path<String> email = entity.get("email");
                where.add(cb.equal(email, filter.getEmail()));
            }
            // filter by username?
            if (filter.getUsername() != null) {
                Path<String> username = entity.get("username");
                where.add(cb.equal(username, filter.getUsername()));
            }
            // filter by password?
            if (filter.getPassword() != null) {
                Path<String> password = entity.get("password");
                where.add(cb.equal(password, filter.getPassword()));
            }
            

            // add where clause
            cq.where(cb.and(where.toArray(new Predicate[where.size()])));

            // compile query
            TypedQuery<Customers> typedQuery = em.createQuery(cq);

            // get list
            List<Customers> resultList = typedQuery.getResultList();

            // build result object
            return resultList;
        }
}
