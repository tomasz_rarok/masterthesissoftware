package com.tomaszrarok.generated.database;

import com.tomaszrarok.database.BaseModelDAO;
import com.tomaszrarok.generated.model.Orders;
import com.tomaszrarok.generated.model.Customers;
import com.tomaszrarok.generated.filter.OrdersFilter;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Data access class for orders
 */
public class Orderss extends BaseModelDAO<Orders> {

    /**
     * Creates a new instance of orders DAO.
     */
    public Orderss() {
        super(Orders.class);
    }

    /**
     * Test if the orders with the given id exists.
     *
     * @param id the id of the orders
     * @return true if orders exists
     */
    public boolean exists(Long id) {

        // check database connection
        assert em.isOpen();

        // query for elements
        Query q = em.createQuery("SELECT a FROM Orders a WHERE "
                + "a.id = :id");
        q.setParameter("id", id);

        // return result
        return !q.getResultList().isEmpty();
    }


    /**
     * Removes a orders and unbounds it from foreign relations.
     *
     * @param orders the orders
     */
    @Override
    public void delete(Orders orders) {


        // backup primary key
        Long id = orders.getId();

        // update entity and remove
        orders = get(id);
        if (orders != null) super.delete(orders);
    }

    public List<Orders> list(OrdersFilter filter) {
            // check database connection
            assert em.isOpen();

            // build query using criteria API
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery cq = cb.createQuery();

            // get root
            Root<Orders> entity = cq.from(Orders.class);

            // prepare where list
            ArrayList<Predicate> where = new ArrayList<>();

            // filter by id?
            if (filter.getId() != null) {
                Path<Long> id = entity.get("id");
                where.add(cb.equal(id, filter.getId()));
            }

            // filter by name?
            if (filter.getName() != null) {
                Path<String> name = entity.get("name");
                where.add(cb.equal(name, filter.getName()));
            }
            // filter by customers?
            if (filter.getCustomers() != null) {
                Path<Customers> customers = entity.get("customers");
                where.add(cb.equal(customers, filter.getCustomers()));
            }
            

            // add where clause
            cq.where(cb.and(where.toArray(new Predicate[where.size()])));

            // compile query
            TypedQuery<Orders> typedQuery = em.createQuery(cq);

            // get list
            List<Orders> resultList = typedQuery.getResultList();

            // build result object
            return resultList;
        }
}
