package com.tomaszrarok.generated.database;

import com.tomaszrarok.database.BaseModelDAO;
import com.tomaszrarok.generated.model.Orderproduct;
import com.tomaszrarok.generated.model.Products;import com.tomaszrarok.generated.model.Orders;
import com.tomaszrarok.generated.filter.OrderproductFilter;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Data access class for orderproduct
 */
public class Orderproducts extends BaseModelDAO<Orderproduct> {

    /**
     * Creates a new instance of orderproduct DAO.
     */
    public Orderproducts() {
        super(Orderproduct.class);
    }

    /**
     * Test if the orderproduct with the given id exists.
     *
     * @param id the id of the orderproduct
     * @return true if orderproduct exists
     */
    public boolean exists(Long id) {

        // check database connection
        assert em.isOpen();

        // query for elements
        Query q = em.createQuery("SELECT a FROM Orderproduct a WHERE "
                + "a.id = :id");
        q.setParameter("id", id);

        // return result
        return !q.getResultList().isEmpty();
    }


    /**
     * Removes a orderproduct and unbounds it from foreign relations.
     *
     * @param orderproduct the orderproduct
     */
    @Override
    public void delete(Orderproduct orderproduct) {


        // backup primary key
        Long id = orderproduct.getId();

        // update entity and remove
        orderproduct = get(id);
        if (orderproduct != null) super.delete(orderproduct);
    }

    public List<Orderproduct> list(OrderproductFilter filter) {
            // check database connection
            assert em.isOpen();

            // build query using criteria API
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery cq = cb.createQuery();

            // get root
            Root<Orderproduct> entity = cq.from(Orderproduct.class);

            // prepare where list
            ArrayList<Predicate> where = new ArrayList<>();

            // filter by id?
            if (filter.getId() != null) {
                Path<Long> id = entity.get("id");
                where.add(cb.equal(id, filter.getId()));
            }

            // filter by name?
            if (filter.getName() != null) {
                Path<Double> name = entity.get("name");
                where.add(cb.equal(name, filter.getName()));
            }
            // filter by products?
            if (filter.getProducts() != null) {
                Path<Products> products = entity.get("products");
                where.add(cb.equal(products, filter.getProducts()));
            }
            // filter by orders?
            if (filter.getOrders() != null) {
                Path<Orders> orders = entity.get("orders");
                where.add(cb.equal(orders, filter.getOrders()));
            }
            

            // add where clause
            cq.where(cb.and(where.toArray(new Predicate[where.size()])));

            // compile query
            TypedQuery<Orderproduct> typedQuery = em.createQuery(cq);

            // get list
            List<Orderproduct> resultList = typedQuery.getResultList();

            // build result object
            return resultList;
        }
}
