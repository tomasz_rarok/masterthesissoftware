package com.tomaszrarok.generated.database;

import com.tomaszrarok.database.BaseModelDAO;
import com.tomaszrarok.generated.model.Products;
import com.tomaszrarok.generated.model.Category;
import com.tomaszrarok.generated.filter.ProductsFilter;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Data access class for products
 */
public class Productss extends BaseModelDAO<Products> {

    /**
     * Creates a new instance of products DAO.
     */
    public Productss() {
        super(Products.class);
    }

    /**
     * Test if the products with the given id exists.
     *
     * @param id the id of the products
     * @return true if products exists
     */
    public boolean exists(Long id) {

        // check database connection
        assert em.isOpen();

        // query for elements
        Query q = em.createQuery("SELECT a FROM Products a WHERE "
                + "a.id = :id");
        q.setParameter("id", id);

        // return result
        return !q.getResultList().isEmpty();
    }


    /**
     * Removes a products and unbounds it from foreign relations.
     *
     * @param products the products
     */
    @Override
    public void delete(Products products) {


        // backup primary key
        Long id = products.getId();

        // update entity and remove
        products = get(id);
        if (products != null) super.delete(products);
    }

    public List<Products> list(ProductsFilter filter) {
            // check database connection
            assert em.isOpen();

            // build query using criteria API
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery cq = cb.createQuery();

            // get root
            Root<Products> entity = cq.from(Products.class);

            // prepare where list
            ArrayList<Predicate> where = new ArrayList<>();

            // filter by id?
            if (filter.getId() != null) {
                Path<Long> id = entity.get("id");
                where.add(cb.equal(id, filter.getId()));
            }

            // filter by name?
            if (filter.getName() != null) {
                Path<String> name = entity.get("name");
                where.add(cb.equal(name, filter.getName()));
            }
            // filter by price?
            if (filter.getPrice() != null) {
                Path<Double> price = entity.get("price");
                where.add(cb.equal(price, filter.getPrice()));
            }
            // filter by category?
            if (filter.getCategory() != null) {
                Path<Category> category = entity.get("category");
                where.add(cb.equal(category, filter.getCategory()));
            }
            

            // add where clause
            cq.where(cb.and(where.toArray(new Predicate[where.size()])));

            // compile query
            TypedQuery<Products> typedQuery = em.createQuery(cq);

            // get list
            List<Products> resultList = typedQuery.getResultList();

            // build result object
            return resultList;
        }
}
