package com.tomaszrarok.generated.validator;

import com.tomaszrarok.generated.model.Category;
import com.tomaszrarok.validator.BaseModelValidator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* Validator class for Categorys.
 */
public class CategoryValidator implements BaseModelValidator<Category> {

    /**
     * Validates a given Category and returns a map of errors.
     * @param category the Category to validate
     * @return a map of errors (keys contain attribute names, values a list of error message)
     */
    @Override
    public Map<String, List<String>> validate(Category category) {

        // init error map and list
        HashMap<String, List<String>> errors = new HashMap<>();
        List<String> errorMessages;

        // check if object is null
        if (category == null) {
            errorMessages = new ArrayList<>();
            errorMessages.add("Object is null");
            errors.put("object", errorMessages);
            return errors;
        }

        errorMessages = new ArrayList<>();
         // checkName
        if (category.getName() == null) {
            errorMessages.add("category.name.errors.required");
        }

        if (!errorMessages.isEmpty()) {
            errors.put("name", errorMessages);
        }
        

        // return validation result
        return errors;
    }
}
