package com.tomaszrarok.generated.validator;

import com.tomaszrarok.generated.model.Products;
import com.tomaszrarok.validator.BaseModelValidator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* Validator class for Productss.
 */
public class ProductsValidator implements BaseModelValidator<Products> {

    /**
     * Validates a given Products and returns a map of errors.
     * @param products the Products to validate
     * @return a map of errors (keys contain attribute names, values a list of error message)
     */
    @Override
    public Map<String, List<String>> validate(Products products) {

        // init error map and list
        HashMap<String, List<String>> errors = new HashMap<>();
        List<String> errorMessages;

        // check if object is null
        if (products == null) {
            errorMessages = new ArrayList<>();
            errorMessages.add("Object is null");
            errors.put("object", errorMessages);
            return errors;
        }

        errorMessages = new ArrayList<>();
         // checkName
        if (products.getName() == null) {
            errorMessages.add("products.name.errors.required");
        }

        if (!errorMessages.isEmpty()) {
            errors.put("name", errorMessages);
        }
         // checkPrice
        if (products.getPrice() == null) {
            errorMessages.add("products.price.errors.required");
        }

        if (!errorMessages.isEmpty()) {
            errors.put("price", errorMessages);
        }
         // checkCategory
        if (products.getCategory() == null) {
            errorMessages.add("products.category.errors.required");
        }

        if (!errorMessages.isEmpty()) {
            errors.put("category", errorMessages);
        }
        

        // return validation result
        return errors;
    }
}
