package com.tomaszrarok.generated.validator;

import com.tomaszrarok.generated.model.Orderproduct;
import com.tomaszrarok.validator.BaseModelValidator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* Validator class for Orderproducts.
 */
public class OrderproductValidator implements BaseModelValidator<Orderproduct> {

    /**
     * Validates a given Orderproduct and returns a map of errors.
     * @param orderproduct the Orderproduct to validate
     * @return a map of errors (keys contain attribute names, values a list of error message)
     */
    @Override
    public Map<String, List<String>> validate(Orderproduct orderproduct) {

        // init error map and list
        HashMap<String, List<String>> errors = new HashMap<>();
        List<String> errorMessages;

        // check if object is null
        if (orderproduct == null) {
            errorMessages = new ArrayList<>();
            errorMessages.add("Object is null");
            errors.put("object", errorMessages);
            return errors;
        }

        errorMessages = new ArrayList<>();
         // checkName
        if (orderproduct.getName() == null) {
            errorMessages.add("orderproduct.name.errors.required");
        }

        if (!errorMessages.isEmpty()) {
            errors.put("name", errorMessages);
        }
         // checkProducts
        if (orderproduct.getProducts() == null) {
            errorMessages.add("orderproduct.products.errors.required");
        }

        if (!errorMessages.isEmpty()) {
            errors.put("products", errorMessages);
        }
         // checkOrders
        if (orderproduct.getOrders() == null) {
            errorMessages.add("orderproduct.orders.errors.required");
        }

        if (!errorMessages.isEmpty()) {
            errors.put("orders", errorMessages);
        }
        

        // return validation result
        return errors;
    }
}
