package com.tomaszrarok.generated.validator;

import com.tomaszrarok.generated.model.Customers;
import com.tomaszrarok.validator.BaseModelValidator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* Validator class for Customerss.
 */
public class CustomersValidator implements BaseModelValidator<Customers> {

    /**
     * Validates a given Customers and returns a map of errors.
     * @param customers the Customers to validate
     * @return a map of errors (keys contain attribute names, values a list of error message)
     */
    @Override
    public Map<String, List<String>> validate(Customers customers) {

        // init error map and list
        HashMap<String, List<String>> errors = new HashMap<>();
        List<String> errorMessages;

        // check if object is null
        if (customers == null) {
            errorMessages = new ArrayList<>();
            errorMessages.add("Object is null");
            errors.put("object", errorMessages);
            return errors;
        }

        errorMessages = new ArrayList<>();
         // checkFirstname
        if (customers.getFirstname() == null) {
            errorMessages.add("customers.firstname.errors.required");
        }

        if (!errorMessages.isEmpty()) {
            errors.put("firstname", errorMessages);
        }
         // checkEmail
        if (customers.getEmail() == null) {
            errorMessages.add("customers.email.errors.required");
        }

        if (!errorMessages.isEmpty()) {
            errors.put("email", errorMessages);
        }
         // checkUsername
        if (customers.getUsername() == null) {
            errorMessages.add("customers.username.errors.required");
        }

        if (!errorMessages.isEmpty()) {
            errors.put("username", errorMessages);
        }
         // checkPassword
        if (customers.getPassword() == null) {
            errorMessages.add("customers.password.errors.required");
        }

        if (!errorMessages.isEmpty()) {
            errors.put("password", errorMessages);
        }
        

        // return validation result
        return errors;
    }
}
