package com.tomaszrarok.generated.validator;

import com.tomaszrarok.generated.model.Orders;
import com.tomaszrarok.validator.BaseModelValidator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* Validator class for Orderss.
 */
public class OrdersValidator implements BaseModelValidator<Orders> {

    /**
     * Validates a given Orders and returns a map of errors.
     * @param orders the Orders to validate
     * @return a map of errors (keys contain attribute names, values a list of error message)
     */
    @Override
    public Map<String, List<String>> validate(Orders orders) {

        // init error map and list
        HashMap<String, List<String>> errors = new HashMap<>();
        List<String> errorMessages;

        // check if object is null
        if (orders == null) {
            errorMessages = new ArrayList<>();
            errorMessages.add("Object is null");
            errors.put("object", errorMessages);
            return errors;
        }

        errorMessages = new ArrayList<>();
         // checkName
        if (orders.getName() == null) {
            errorMessages.add("orders.name.errors.required");
        }

        if (!errorMessages.isEmpty()) {
            errors.put("name", errorMessages);
        }
         // checkCustomers
        if (orders.getCustomers() == null) {
            errorMessages.add("orders.customers.errors.required");
        }

        if (!errorMessages.isEmpty()) {
            errors.put("customers", errorMessages);
        }
        

        // return validation result
        return errors;
    }
}
