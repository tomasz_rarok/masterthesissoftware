package com.tomaszrarok.generated.filter;

import javax.persistence.*;
import com.tomaszrarok.filter.BaseFilter;


/**
 * Filter class for category
 */


public class CategoryFilter extends BaseFilter  {
 @Column(name = "name")
  private String name;

  
  
  /**
   * Getter for name
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * Setter for name
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }
  
}