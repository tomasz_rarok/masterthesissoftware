package com.tomaszrarok.generated.filter;

import javax.persistence.*;
import com.tomaszrarok.filter.BaseFilter;
import com.tomaszrarok.generated.model.Customers;

/**
 * Filter class for orders
 */


public class OrdersFilter extends BaseFilter  {
 @Column(name = "name")
  private String name;

  @Column(name = "customers")
  private Customers customers;

  
  
  /**
   * Getter for name
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * Setter for name
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }
  
  /**
   * Getter for customers
   * @return the customers
   */
  public Customers getCustomers() {
    return customers;
  }

  /**
   * Setter for customers
   * @param customers the customers to set
   */
  public void setCustomers(Customers customers) {
    this.customers = customers;
  }
  
}