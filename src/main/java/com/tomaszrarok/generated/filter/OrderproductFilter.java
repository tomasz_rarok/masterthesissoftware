package com.tomaszrarok.generated.filter;

import javax.persistence.*;
import com.tomaszrarok.filter.BaseFilter;
import com.tomaszrarok.generated.model.Products;import com.tomaszrarok.generated.model.Orders;

/**
 * Filter class for orderproduct
 */


public class OrderproductFilter extends BaseFilter  {
 @Column(name = "name")
  private Double name;

  @Column(name = "products")
  private Products products;

  @Column(name = "orders")
  private Orders orders;

  
  
  /**
   * Getter for name
   * @return the name
   */
  public Double getName() {
    return name;
  }

  /**
   * Setter for name
   * @param name the name to set
   */
  public void setName(Double name) {
    this.name = name;
  }
  
  /**
   * Getter for products
   * @return the products
   */
  public Products getProducts() {
    return products;
  }

  /**
   * Setter for products
   * @param products the products to set
   */
  public void setProducts(Products products) {
    this.products = products;
  }
  
  /**
   * Getter for orders
   * @return the orders
   */
  public Orders getOrders() {
    return orders;
  }

  /**
   * Setter for orders
   * @param orders the orders to set
   */
  public void setOrders(Orders orders) {
    this.orders = orders;
  }
  
}