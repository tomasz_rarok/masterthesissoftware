package com.tomaszrarok.validator;

import com.tomaszrarok.model.BaseModel;

import java.util.List;
import java.util.Map;

public interface BaseModelValidator<T extends BaseModel> {

    public abstract Map<String, List<String>> validate(T item);

}
