package com.tomaszrarok;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("rest")
public class ApplicationConfig extends Application {
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> s = new HashSet<Class<?>>();
        s.add(com.tomaszrarok.controller.CustomersResource.class);
        
        s.add(com.tomaszrarok.controller.ProductsResource.class);
        
        s.add(com.tomaszrarok.controller.CategoryResource.class);
        
        s.add(com.tomaszrarok.controller.OrdersResource.class);
        
        s.add(com.tomaszrarok.controller.OrderproductResource.class);
        
        return s;
    }
}