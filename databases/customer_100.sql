#
# TABLE STRUCTURE FOR: customer
#

DROP TABLE IF EXISTS customer;

CREATE TABLE `customer` (
  `id` bigint(20) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (0, '1984-02-05 15:41:16', 'hlangosh@example.org', 'Xzavier', 'Pollich', '1974-03-23 08:33:38');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (1, '1995-01-25 01:27:26', 'whitney.torp@example.org', 'Izabella', 'Dicki', '1977-07-10 19:20:54');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (2, '2016-07-20 20:00:44', 'rhianna.hodkiewicz@example.com', 'Lera', 'McDermott', '2000-10-23 02:32:23');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (3, '1984-03-30 17:01:11', 'retta52@example.net', 'Declan', 'Rippin', '1979-09-25 23:50:10');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (4, '1979-01-07 02:56:39', 'mireille29@example.org', 'Cicero', 'Funk', '1995-12-13 18:04:13');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (5, '2015-03-05 16:50:55', 'klangworth@example.com', 'Cierra', 'Mayert', '1991-07-13 04:30:37');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (6, '1974-12-31 22:55:26', 'funk.sean@example.org', 'Ruben', 'Lubowitz', '1983-08-03 17:52:42');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (7, '1994-10-06 04:28:19', 'sandy.kirlin@example.com', 'Maegan', 'Hirthe', '1987-11-11 16:06:03');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (9, '1970-01-05 17:02:31', 'hbecker@example.net', 'Madonna', 'Robel', '1992-11-21 23:47:46');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (10, '2014-09-21 09:45:16', 'schneider.cyril@example.org', 'Ambrose', 'Koss', '1982-06-03 23:32:04');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (11, '2002-06-28 13:51:44', 'kgutkowski@example.com', 'Jonatan', 'Mohr', '1978-04-03 20:07:32');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (12, '1994-11-26 02:47:53', 'syble14@example.net', 'Lloyd', 'Feil', '2005-03-25 19:11:38');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (13, '1983-08-08 16:34:47', 'vgerlach@example.com', 'Ramona', 'Kuphal', '1973-05-15 05:41:06');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (14, '2000-05-19 21:01:59', 'bessie.kessler@example.net', 'Casandra', 'Trantow', '1983-07-16 06:04:30');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (15, '2016-07-22 19:22:06', 'nhintz@example.net', 'Idella', 'Harris', '1988-02-13 15:12:56');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (16, '1973-09-16 05:20:04', 'trycia65@example.org', 'Kasey', 'Runolfsdottir', '1998-05-09 23:22:56');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (17, '1988-06-22 15:16:59', 'loraine.cormier@example.com', 'Lesly', 'Parisian', '2001-02-22 19:37:17');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (18, '2005-03-20 14:27:59', 'cormier.stone@example.org', 'Ewald', 'Heller', '1978-09-02 09:29:26');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (19, '1998-04-16 09:06:02', 'kmetz@example.net', 'Laurie', 'Sipes', '2015-07-16 23:55:33');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (20, '1996-08-08 19:35:09', 'anabel25@example.com', 'Ena', 'Jast', '2000-08-17 20:17:34');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (21, '1998-03-07 02:37:11', 'gwisoky@example.org', 'Jamil', 'Wuckert', '2001-10-25 11:30:55');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (22, '2003-02-10 04:32:25', 'koelpin.nedra@example.com', 'Eula', 'McClure', '2008-08-26 17:24:22');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (23, '1973-03-05 10:17:07', 'sasha37@example.org', 'Rachelle', 'Johns', '1983-04-17 05:53:42');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (24, '2005-10-10 00:40:26', 'qerdman@example.com', 'Bridie', 'Daugherty', '1994-10-06 13:12:43');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (25, '2009-04-11 22:38:54', 'mikel.rath@example.com', 'Eloisa', 'Zieme', '1972-10-09 05:11:51');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (26, '1970-11-07 07:54:01', 'jones.maryjane@example.com', 'Viola', 'Collier', '1983-07-14 09:30:16');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (27, '2005-04-10 13:08:43', 'zmoore@example.com', 'Barrett', 'King', '1989-02-11 17:24:01');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (28, '1972-01-05 04:23:44', 'zbednar@example.net', 'Corene', 'Daugherty', '1992-12-02 21:03:19');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (29, '1994-01-30 17:48:00', 'pryan@example.org', 'Anahi', 'Daniel', '1999-05-08 12:05:33');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (30, '2008-04-10 18:59:36', 'ardella97@example.net', 'Dan', 'Rogahn', '1999-01-07 08:16:00');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (31, '2016-09-21 17:52:55', 'bernser@example.com', 'Jack', 'Swaniawski', '1974-06-05 08:24:00');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (32, '1993-03-26 16:26:01', 'hal25@example.net', 'Tanya', 'Thiel', '1997-03-03 20:56:10');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (33, '1995-06-28 20:58:10', 'elmira76@example.net', 'Katheryn', 'Bergstrom', '1977-02-16 11:50:58');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (34, '2000-06-20 18:30:47', 'hettinger.myles@example.net', 'Arnold', 'Quigley', '1996-03-15 11:23:22');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (35, '2007-03-06 20:05:06', 'wehner.maurice@example.net', 'Adeline', 'Medhurst', '1975-06-21 13:32:06');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (36, '2001-01-18 09:56:06', 'franecki.irwin@example.org', 'Jimmie', 'Aufderhar', '2014-07-20 10:08:54');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (37, '1970-06-04 18:04:54', 'shaina65@example.org', 'Gianni', 'Volkman', '1981-09-04 08:54:58');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (38, '1998-09-07 21:23:44', 'vo\'kon@example.com', 'Damien', 'Gottlieb', '1978-02-20 20:18:00');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (39, '1987-09-25 12:59:42', 'aron72@example.org', 'Carter', 'Parisian', '1991-04-25 14:58:08');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (40, '2007-02-25 14:29:33', 'asa.mcclure@example.org', 'Jacklyn', 'Herzog', '1983-11-20 19:18:58');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (41, '1991-09-03 03:53:22', 'lavinia.kemmer@example.org', 'Tod', 'Carroll', '2002-07-21 17:36:56');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (42, '1994-11-08 01:08:08', 'brycen85@example.net', 'Olin', 'Luettgen', '1972-02-22 21:14:23');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (43, '2011-12-22 10:10:57', 'audreanne.cartwright@example.org', 'Burdette', 'Kub', '1971-08-22 15:17:50');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (44, '2000-08-21 16:42:04', 'toney78@example.net', 'Creola', 'Schroeder', '1983-09-10 13:46:49');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (45, '2004-07-03 08:38:35', 'prohaska.torey@example.net', 'Emmanuelle', 'Bartoletti', '2011-09-06 03:12:50');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (46, '1997-10-27 17:30:22', 'xlakin@example.org', 'Westley', 'Schoen', '1992-05-05 07:04:24');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (47, '2016-02-21 02:16:04', 'mekhi93@example.net', 'Freda', 'Gaylord', '2011-02-27 03:07:20');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (48, '1989-07-10 10:12:19', 'ddach@example.org', 'Mathias', 'McClure', '1986-09-02 20:19:17');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (49, '1999-10-28 07:01:56', 'rosalee97@example.net', 'Domenick', 'Blick', '1995-07-03 21:15:04');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (50, '1999-07-18 22:14:47', 'sierra.conroy@example.net', 'Lucio', 'Walter', '1996-11-07 20:15:53');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (51, '2015-01-04 04:24:30', 'douglas.monserrate@example.net', 'Consuelo', 'Dare', '1984-12-30 02:12:42');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (52, '1992-06-29 04:32:16', 'qstokes@example.org', 'Cecil', 'Hilll', '2014-08-28 05:29:53');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (53, '2015-05-16 18:01:55', 'rita.balistreri@example.com', 'Malinda', 'Bernhard', '1990-08-29 21:07:55');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (54, '1987-04-25 04:35:43', 'pagac.joshua@example.org', 'Giles', 'Gleichner', '2010-03-24 03:51:11');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (55, '2017-02-16 09:59:50', 'williamson.helga@example.com', 'Sasha', 'Grant', '1993-10-19 19:48:30');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (56, '2011-08-15 12:55:03', 'gsawayn@example.org', 'Gabe', 'Stoltenberg', '2009-06-16 12:40:07');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (57, '2000-01-20 15:22:03', 'hellen62@example.com', 'Lambert', 'Mosciski', '1982-12-17 07:32:48');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (58, '1976-08-17 11:08:49', 'xbernier@example.org', 'Rosie', 'Rutherford', '2013-10-04 05:01:00');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (59, '1988-01-28 23:19:44', 'rutherford.dell@example.net', 'Miracle', 'Green', '1983-07-25 03:10:04');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (60, '1980-10-28 21:05:46', 'bins.ila@example.net', 'Darian', 'Paucek', '2000-08-26 06:15:50');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (61, '1974-05-03 18:15:11', 'rbogan@example.org', 'Brenda', 'Rodriguez', '1976-04-24 18:37:08');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (62, '2002-10-11 16:57:57', 'adan.yost@example.net', 'Ewell', 'Skiles', '1971-07-25 03:25:11');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (63, '1987-03-01 18:57:03', 'abbott.lucile@example.org', 'Name', 'Turcotte', '2015-06-16 11:18:42');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (64, '2011-09-12 07:12:10', 'shills@example.net', 'Nichole', 'Johns', '1979-11-22 05:33:31');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (65, '1977-07-03 13:54:42', 'robel.dina@example.org', 'Pierce', 'Murray', '1998-03-22 00:36:41');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (66, '1970-09-16 13:39:40', 'hermann.griffin@example.org', 'Alfonzo', 'West', '1990-06-30 19:49:15');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (67, '2008-12-29 19:22:18', 'ayana45@example.net', 'Osbaldo', 'Thiel', '1996-04-17 22:41:49');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (68, '1988-03-04 10:13:23', 'claudie94@example.com', 'Hans', 'Langosh', '2005-02-28 18:34:56');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (69, '2015-07-05 02:49:22', 'gleichner.durward@example.org', 'Nico', 'Cassin', '1984-01-15 21:38:38');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (70, '2007-11-29 08:41:17', 'heloise.marvin@example.net', 'Shayne', 'Stroman', '1981-01-30 04:10:05');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (71, '1978-06-22 20:22:08', 'harrison32@example.org', 'Cathryn', 'Fahey', '1991-01-13 03:20:56');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (72, '2010-03-08 21:54:32', 'moshe.kub@example.net', 'Krista', 'Feest', '1998-04-25 00:50:04');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (73, '2000-03-03 22:51:42', 'vivianne.kris@example.org', 'Jerrell', 'Hartmann', '1991-08-07 02:41:57');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (74, '1992-06-03 01:46:02', 'turner.norberto@example.org', 'Beaulah', 'Hagenes', '1972-02-20 08:32:10');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (75, '1996-09-22 05:39:17', 'hills.colt@example.com', 'Coty', 'Witting', '1978-09-27 02:13:55');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (76, '1995-04-14 20:45:17', 'chloe06@example.net', 'Carmine', 'Bailey', '2006-07-07 18:14:21');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (77, '2007-05-09 13:20:22', 'abraham60@example.com', 'Jared', 'Kihn', '1990-04-03 22:30:53');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (78, '1973-03-08 10:48:01', 'dreinger@example.com', 'Crystal', 'Bergstrom', '1992-11-12 19:51:20');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (79, '1986-10-23 21:13:41', 'zharber@example.com', 'Emmalee', 'Halvorson', '1996-07-07 01:54:53');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (80, '1970-02-19 21:31:08', 'adelbert10@example.org', 'Karine', 'Bradtke', '2008-10-30 04:33:14');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (81, '1990-10-04 15:58:09', 'yraynor@example.net', 'Kyra', 'King', '2007-04-17 04:06:09');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (82, '1988-01-28 21:01:36', 'vpadberg@example.org', 'Chaim', 'Will', '1991-12-11 02:34:56');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (83, '2010-01-05 02:08:03', 'chintz@example.org', 'Kaleigh', 'Hackett', '1984-01-14 13:22:14');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (84, '2015-03-20 18:48:09', 'rnienow@example.org', 'Jennyfer', 'Hessel', '1980-12-07 14:41:49');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (85, '1977-02-18 04:37:22', 'gustave98@example.org', 'Russ', 'DuBuque', '1995-05-02 09:43:17');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (86, '2004-02-27 19:54:46', 'gwalter@example.net', 'Quincy', 'Bednar', '1971-10-07 02:29:19');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (87, '1999-10-09 20:14:56', 'kkertzmann@example.org', 'Ervin', 'Kiehn', '2002-10-21 23:53:26');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (88, '1994-11-26 11:54:39', 'gilda.o\'keefe@example.net', 'Ryann', 'Graham', '1972-09-09 02:00:10');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (89, '2017-10-29 18:39:04', 'murphy.hammes@example.net', 'Rowan', 'Padberg', '2014-09-17 11:32:52');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (90, '1985-04-29 12:33:43', 'hayden71@example.org', 'Kurtis', 'Kihn', '1983-12-08 18:22:18');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (91, '2012-07-29 04:29:54', 'alexane.simonis@example.net', 'Adrian', 'Von', '2010-01-09 10:44:55');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (92, '2012-01-17 09:56:37', 'ayla68@example.com', 'Roma', 'Corwin', '1987-04-18 08:02:03');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (93, '2003-02-26 05:59:07', 'collins.garett@example.net', 'Maureen', 'Johnson', '2010-03-20 05:13:22');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (94, '2002-10-14 03:43:55', 'desmond16@example.org', 'Gilbert', 'Lockman', '1989-05-25 16:12:48');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (95, '1987-07-06 13:26:37', 'weston.bednar@example.com', 'Clemens', 'Smith', '1999-08-03 22:27:13');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (96, '2000-09-10 17:14:22', 'bailey99@example.com', 'Josue', 'Roberts', '1977-09-14 02:28:12');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (97, '2017-09-28 10:51:56', 'emily.nikolaus@example.com', 'Cecile', 'Marvin', '1990-07-29 13:46:39');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (98, '2001-11-30 02:26:48', 'efrain12@example.org', 'Muhammad', 'Sawayn', '1992-07-17 02:28:36');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (99, '2007-05-22 17:52:10', 'vhermiston@example.net', 'Rosella', 'Heaney', '1992-03-15 01:44:35');
INSERT INTO customer (`id`, `create_date`, `email`, `firstname`, `lastname`, `modify_date`) VALUES (100, '2014-12-24 02:42:35', 'leon74@example.net', 'Jasper', 'Cruickshank', '1986-06-19 05:12:42');


