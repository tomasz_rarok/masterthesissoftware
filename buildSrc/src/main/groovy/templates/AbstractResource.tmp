package <%= project.packageName %>.generated.controller;

import <%= project.packageName %>.controller.BaseResource;
import <%= project.packageName %>.generated.database.<%= model.name.capitalize() %>s;
import <%= project.packageName %>.generated.model.<%= model.name.capitalize() %>;
import <%= project.packageName %>.generated.validator.<%= model.name.capitalize() %>Validator;
import <%= project.packageName %>.generated.filter.<%= model.name.capitalize() %>Filter;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * REST API for <%= model.name.capitalize() %>
 */
public abstract class Abstract<%= model.name.capitalize() %>Resource extends BaseResource {

    /**
     * Get a list of <%= model.name %>s.
     *
     * @param request the current HTTP request
     * @return a list of <%= model.name %>s
     */
    @GET
    public List<<%= model.name.capitalize() %>> index(@Context HttpServletRequest request) {

        try (<%= model.name.capitalize() %>s <%= model.name %>s = new <%= model.name.capitalize() %>s()) {
            return <%= model.name %>s.list();
        }
    }

    /**
     * Get a filtered list of  <%= model.name %>s.
     * @param filter the <%= model.name %> filter
     * @param request the current HTTP request
     * @return a list of <%= model.name %>
     */
    @POST
    @Path("/filter")
    public List<<%= model.name.capitalize() %>> filter(<%= model.name.capitalize() %>Filter filter,
                                         @Context HttpServletRequest request) {

        try (<%= model.name.capitalize() %>s <%= model.name %>s= new <%= model.name.capitalize() %>s ()) {

            return <%= model.name %>s.list(filter);
        }
    }

    /**
     * Get a single <%= model.name %>.
     *
     * @param id      the id of the <%= model.name %>
     * @param request the current HTTP request
     * @return the <%= model.name %>
     */
    @GET
    @Path("/{id}")
    public <%= model.name.capitalize() %> details(@PathParam("id") Long id,
                            @Context HttpServletRequest request) {

        try (<%= model.name.capitalize() %>s <%= model.name %>s = new <%= model.name.capitalize() %>s()) {
            return <%= model.name %>s.get(id);
        }
    }

    /**
     * Creates a new <%= model.name %>.
     *
     * @param <%= model.name %> the new <%= model.name %>
     * @param request  the current HTTP request
     * @return the created <%= model.name %>
     */
    @POST
    public <%= model.name.capitalize() %> create(<%= model.name.capitalize() %> <%= model.name %>,
                           @Context HttpServletRequest request) {
        // validate input
        <%= model.name.capitalize() %>Validator <%= model.name %>Validator = new <%= model.name.capitalize() %>Validator();
        Map<String, List<String>> errors = <%= model.name %>Validator.validate(<%= model.name %>);

        if (!errors.isEmpty()) {
            throw new WebApplicationException(getExceptionResponse(errors.toString()));
        }

        // if item exists, do update
        boolean exists = false;
        if (<%= model.name %>.getId() != null) {
            try (<%= model.name.capitalize() %>s <%= model.name %>s = new <%= model.name.capitalize() %>s();) {
                exists = <%= model.name %>s.exists(<%= model.name %>.getId());
            }
        }

        if (exists) {
            return update(<%= model.name %>.getId(), <%= model.name %>, request);
        }

        // ... else create a new one
        try (<%= model.name.capitalize() %>s <%= model.name %>s = new <%= model.name.capitalize() %>s();) {
            <%= model.name %>.setCreateDate(new Date());
            <%= model.name %>.setModifyDate(<%= model.name %>.getCreateDate());
            // save and return result
            return <%= model.name %>s.save(<%= model.name %>);
        }
    }

    /**
     * Saves a changed <%= model.name %>.
     *
     * @param id       the id of the <%= model.name %>
     * @param <%= model.name %> the updated contract
     * @param request  the current HTTP request
     * @return the updated <%= model.name %> object
     */
    @PUT
    @Path("/{id}")
    public <%= model.name.capitalize() %> update(@PathParam("id") Long id, <%= model.name.capitalize() %> <%= model.name %>,
                           @Context HttpServletRequest request) {

        try (<%= model.name.capitalize() %>s <%= model.name %>s = new <%= model.name.capitalize() %>s();) {

            // validate input
            <%= model.name.capitalize() %>Validator <%= model.name %>Validator = new <%= model.name.capitalize() %>Validator();
            Map<String, List<String>> errors = <%= model.name %>Validator.validate(<%= model.name %>);

            if (!errors.isEmpty()) {
                throw new WebApplicationException(getExceptionResponse(errors.toString()));
            }

            // check if item exists
            <%= model.name.capitalize() %> db<%= model.name.capitalize() %> = <%= model.name %>s.get(id);

            // copy data to database item

             <% for (attribute in model.attributes) { %> db<%= model.name.capitalize() %>.set<%= attribute.name.capitalize() %>(<%= model.name %>.get<%= attribute.name.capitalize() %>());
             <% } %>
            db<%= model.name.capitalize() %>.setModifyDate(new Date());

            //save and return result
            return <%= model.name %>s.save(db<%= model.name.capitalize() %>);

        }
    }

    /**
     * Deletes a <%= model.name %> by id.
     *
     * @param id      the uuid of the contract
     * @param request the current HTTP request
     */
    @DELETE
    @Path("/{id}")
    public void delete(@PathParam("id") Long id, @Context HttpServletRequest request) {
        try (<%= model.name.capitalize() %>s <%= model.name %>s = new <%= model.name.capitalize() %>s()) {

            // get <%= model.name %>
            <%= model.name.capitalize() %> <%= model.name %> = <%= model.name %>s.get(id);

            // delete <%= model.name %>
            <%= model.name %>s.delete(<%= model.name %>);

        }
    }
}