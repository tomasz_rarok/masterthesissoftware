import groovy.json.JsonSlurper
import groovy.json.JsonOutput
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import groovy.text.SimpleTemplateEngine
import groovy.lang.Writable

import java.io.FileWriter;

class GenerateAppFiles extends DefaultTask {
    @TaskAction
    def apply() {
        String sep = System.getProperty("file.separator")
        File modelFile = new File(project.rootDir.getAbsolutePath() + sep + "model" + sep + "masterthesissoftware.json")
        JsonSlurper jsonSlurper = new JsonSlurper()
        Project projectSetup = jsonSlurper.parseText( modelFile.getText() )
        if (!projectSetup.isProjectValid()) {
            println("Project is not valid")
            return
        }
        println JsonOutput.toJson(projectSetup)

        deleteDirectory(new File(project.rootDir.getAbsolutePath() + sep + "src" + sep + "main" + sep + "java" + sep + "com" + sep + "tomaszrarok" + sep + "generated"))

        for (Model model : projectSetup.models) {
            Map<String, Object> binding = ["project": projectSetup, "model": model]
            generateTemplate(binding, "Model", "model", model.name.capitalize() + ".java")
            generateTemplate(binding, "AbstractModel", "model", "Abstract" + model.name.capitalize() + ".java")
            generateTemplate(binding, "AbstractResource", "controller", "Abstract" + model.name.capitalize() + "Resource.java")
            generateTemplate(binding, "Resource", "controller", model.name.capitalize() + "Resource.java", false, false)
            generateTemplate(binding, "DatabaseConnector", "database", model.name.capitalize() + "s.java")
            generateTemplate(binding, "Validator", "validator", model.name.capitalize() + "Validator.java")
            generateTemplate(binding, "Filter", "filter", model.name.capitalize() + "Filter.java")
        }
        Map<String, Object> binding = ["project": projectSetup]
        generateTemplate(binding, "AppConfig", "", "ApplicationConfig.java", true, false)
    }

    void generateTemplate(Map<String, Object> binding, String templateName, String targetPackage, String targetFileName){
        generateTemplate(binding, templateName, targetPackage, targetFileName, false, true)
    }

    void generateTemplate(Map<String, Object> binding, String templateName, String targetPackage, String targetFileName, Boolean override, Boolean parentGenerated) {
        String sep = System.getProperty("file.separator")
        String pathToSave
        if(parentGenerated){
            pathToSave = project.rootDir.getAbsolutePath() + sep + "src" + sep + "main" + sep + "java" + sep + "com" + sep + "tomaszrarok" + sep + "generated" + sep
        } else {
            pathToSave = project.rootDir.getAbsolutePath() + sep + "src" + sep + "main" + sep + "java" + sep + "com" + sep + "tomaszrarok" + sep
        }

        String pathTemplates = project.rootDir.getAbsolutePath() + sep + "buildSrc" + sep + "src" + sep + "main" + sep + "groovy" + sep + "templates" + sep

        File f = new File(pathTemplates + sep + templateName + '.tmp')
        SimpleTemplateEngine engine = new SimpleTemplateEngine()


        Writable result = engine.createTemplate(f).make(binding)

        File target = new File(pathToSave + targetPackage + sep, targetFileName)

        if( override || !target.exists()) {
            println "Writing file " + target.getName() + "..."

            if (!target.isFile()) {
                target.parentFile.mkdirs()
                target.createNewFile()
            }

            FileWriter writer = new FileWriter(target)
            result.writeTo(writer).flush()
        }

    }

    boolean deleteDirectory(File dir) {
        if(! dir.exists() || !dir.isDirectory())    {
            return false
        }

        String[] files = dir.list()
        for(int i = 0; i < files.length; i++)    {
            File f = new File(dir, files[i])
            if(f.isDirectory()) {
                deleteDirectory(f)
            }else   {
                f.delete()
            }
        }
        return dir.delete()
    }
}