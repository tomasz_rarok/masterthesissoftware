class Project{
    String name
    String packageName
    List<Model> models

    private final List<String> standards = Arrays.asList("Integer", "Long", "Float", "Double", "Boolean", "String" )
    private List<String> localModels
    private Map<String, List<String>> referencingForModels

    void init(){
        if (!localModels == null ) {
            return
        }
        localModels = new LinkedList<String>()
        referencingForModels = new HashMap<String, List<String>>()
        for (Model m : models) {
            localModels.add(m.name.capitalize())

            for (Attribute a : m.attributes) {
                if (!isStandardType(a.type) && !a.type.equals()) {
                    if (!referencingForModels.containsKey(m.name)) {
                        referencingForModels.put(m.name, new LinkedList<String>())
                    }
                    if (!referencingForModels.get(m.name).contains(a.type)) {
                        referencingForModels.get(m.name).add(a.type)
                    }
                }
            }
        }
    }

    List<String> getReferencingForModels(String name) {
        init()
        return (referencingForModels.get(name) != null &&  referencingForModels.get(name).size() > 0)   ?  referencingForModels.get(name) : Collections.EMPTY_LIST
    }

    boolean modelExist(String type){
        init()
        return localModels.contains(type)
    }

    boolean isStandardType(String type){
        return standards.contains(type)
    }

    boolean isProjectValid(){
        println("Checking if project model is valid")
        boolean valid = true;
        for(Model model : models){
            valid = isValidModel(model)

            if( valid == false )
            {
                return valid
            }
        }

        return valid
    }

    boolean isValidModel(Model model) {
        boolean valid = true
        if( model == null)
        {
            println("Empty model on the list")
            return false
        }
        if( model.name == null){
            println("Empty model name found")
            return false
        }
        if( model.attributes == null || model.attributes.isEmpty()){
            println("Empty attributes list  found")
            return false
        } else {
            println("Checking  attributes for " + model.name )
            for (Attribute attribute : model.attributes){
                valid = valid == false ? false : isValidAttribute(attribute)
            }
        }
        return valid
    }

    boolean isValidAttribute(Attribute attribute) {
        boolean valid = true
        if( attribute == null)
        {
            println("Empty attribute on the list")
            valid = false
            return valid
        }
        if( attribute.name == null){
            println("Empty attribute name found")
            valid = false
        }
        if( attribute.required == null){
            println("Empty attribute required found")
            valid = false
        }
        if( attribute.type == null){
            println("Empty attribute type found")
            valid = false
        }
        if( !isStandardType(attribute.type) && !modelExist(attribute.type)){
            println("Type dismatch found for " + attribute.type)
            valid = false
        }
        return valid
    }
}